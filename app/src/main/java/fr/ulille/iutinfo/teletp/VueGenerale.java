package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle;
    private String poste;
    private  String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL= getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle=DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner salleSpinner = (Spinner) getActivity().findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> salleAdapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_salles, android.R.layout.simple_spinner_item);
        salleAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        salleSpinner.setAdapter(salleAdapter);

        Spinner postSpinner = (Spinner) getActivity().findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> postAdapter = ArrayAdapter.createFromResource(this.getContext(),R.array.list_postes, android.R.layout.simple_spinner_item);
        postAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        postSpinner.setAdapter(postAdapter);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView tvLogin=(TextView) view.findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        ((Spinner)view.findViewById(R.id.spSalle)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // TODO Q5.b
        update();
        // TODO Q9
    }

    // TODO Q5.a
    private void update() {
        Spinner salleSpinner = (Spinner) getActivity().findViewById(R.id.spSalle);
        Spinner postSpinner = (Spinner) getActivity().findViewById(R.id.spPoste);

        if (salleSpinner.getSelectedItem().toString().equals("Distanciel")) {
            postSpinner.setEnabled(false);
            postSpinner.setVisibility(View.INVISIBLE);
            model.setLocalisation("Distanciel");
        }

        else {
            postSpinner.setEnabled(true);
            postSpinner.setVisibility(View.VISIBLE);
            model.setLocalisation(salleSpinner.getSelectedItem().toString() + " : " + postSpinner.getSelectedItem().toString());
        }
    }

    // TODO Q9
}